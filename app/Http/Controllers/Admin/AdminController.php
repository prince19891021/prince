<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function _construct(){
        $this->middleware('admin');
    }

    public function dashboard(){
        return view('admin.body.dashboard');
    }

    public function banner(){
        return view('admin.body.banner');
    }

    public function dev_option(){
        return view('admin.body.dev_option');
    }

    public function material(){
        return view('admin.body.material');
    }
}
