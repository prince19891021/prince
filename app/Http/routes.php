<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/post', function () {
    return view('post');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/chenggonganli', function () {
    return view('chenggonganli');
});

Route::get('/profile', function () {
    return view('profile');
});

Route::get('/project_manage', function () {
    return view('project_manage');
});

Route::get('/service_list', function () {
    return view('service_list');
});

Route::get('/developer_list', function () {
    return view('developer_list');
});

Route::get('/chenggong_list', function () {
    return view('chenggong_list');
});

Route::get('/admin', 'Adminauth\AuthController@showLoginForm');
Route::post('/login_admin', 'Adminauth\AuthController@login');

Route::group(['middleware'=>['admin']], function () {
    Route::get('/dashboard', 'Admin\AdminController@dashboard');
    Route::get('/logout', 'Adminauth\AuthController@logout');
    Route::get('/banner', 'Admin\AdminController@banner');
    Route::get('/dev_option', 'Admin\AdminController@dev_option');
    Route::get('/material', 'Admin\AdminController@material');
}); 

// Route::get('/create', function () {
//     App\User::create([
//         'name'=>'prince',
//         'username'=>'prince',
//         'email'=>'castillo_d98@yahoo.com',
//         'password'=>bcrypt('prince1021'),
//     ]);
// });

