 <?php $__env->startSection('nav_menu'); ?>
<!-- start navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="position: relative;">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
      aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.php">His <span>Win</span></a>
    <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
  </div>
  <div id="navbar" class="navbar-collapse collapse navbar_area" >
    <ul class="nav navbar-nav navbar-right custom_nav">
      <li><a href="home">Home</a></li>
      <li class="active"><a href="post">Post</a></li>
      <li><a href="login">Login</a></li>
                <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Link one</a></li>
              <li><a href="#">Link Two</a></li>
              <li><a href="#">Link Three</a></li>
            </ul>
          </li>
          <li><a href="page.html">Page</a></li>
          <li><a href="blog-archive.html">Blog</a></li> -->
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </nav>
  <!-- End navbar -->
  <link href="<?php echo e(asset('css/developer_list.css')); ?>" rel="stylesheet">

  <?php $__env->stopSection(); ?>
  <?php $__env->startSection('service'); ?>

  <div class="container-fluid">
    <div class="container container-pad" id="property-listings">
        
      <div class="row">

<br>
        <div class="col-md-12">
          <h1>Our Excellent Developers</h1>
          <p>A snippet I recently used to display homes for a local brokerage.  Focused more on images when accessed through mobile</p>
        </div>

      </div>
      <br>
      <div class="row">
        <div class="col-md-2" >
          <ul class="nav nav-pills nav-stacked">
            <li><a href="#">Android Development</a></li>
            <li><a href="#">iOS Development</a></li>
            <li class="active"><a href="#">Website Building</a></li>
            <li><a href="#">OCR Technology</a></li>
            <li><a href="#">Other Technology</a></li>
          </ul>
        </div>
        <div class="col-md-10">

         <!-- start project list row(table) -->             
         <div class="row">

          <div class="col-sm-12"> 

            <ul class="media-list">
              <li class="media">
                <a class="pull-left">
                  <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/128.jpg" alt="profile">
                </a>
                <div class="media-body col-md-10">
                  <div class="well well-lg" style=" padding-bottom: 10px;">
                  <h3 class="media-heading">
                              <a href="profile" target="_parent"><strong>Marco Jacken</strong></a></h3>
                    <ul class="media-date text-uppercase reviews list-inline">
                      <span class="badge badge-important">Android</span>
                      <span class="badge badge-important">iOS</span>
                      <span class="badge badge-important">c++</span>
                    </ul>
                    <p class="media-comment">
                      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                    </p>
                      <!-- <a class="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
                      <a class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#replyOne"><span class="glyphicon glyphicon-comment"></span> 2 comments</a> -->
                    </div>              
                  </div>
                  
                </li>          
                <li class="media">
                  <a class="pull-left" href="#">
                    <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kurafire/128.jpg" alt="profile">
                  </a>
                  <div class="media-body col-md-10">
                    <div class="well well-lg" style=" padding-bottom: 10px;">
                       <h3 class="media-heading">
                              <a href="profile" target="_parent"><strong>Marco Jacken</strong></a></h3>
                      <ul class="media-date text-uppercase reviews list-inline">
                        <span class="badge badge-important">Android</span>
                        <span class="badge badge-important">iOS</span>
                        <span class="badge badge-important">c++</span>
                      </ul>
                      <p class="media-comment">
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                      </p>
                      <!-- <a class="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
                      <a class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#replyOne"><span class="glyphicon glyphicon-comment"></span> 2 comments</a> -->
                    </div>              
                  </div>
                </li>
                <li class="media">
                  <a class="pull-left">
                    <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/lady_katherine/128.jpg" alt="profile">
                  </a>
                  <div class="media-body col-md-10">
                    <div class="well well-lg" style=" padding-bottom: 10px;">
                       <h3 class="media-heading">
                              <a href="profile" target="_parent"><strong>Marco Jacken</strong></a></h3>
                      <ul class="media-date text-uppercase reviews list-inline">
                        <span class="badge badge-important">Android</span>
                        <span class="badge badge-important">iOS</span>
                        <span class="badge badge-important">c++</span>
                      </ul>
                      <p class="media-comment">
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                      </p>
                      <!-- <a class="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
                      <a class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#replyOne"><span class="glyphicon glyphicon-comment"></span> 2 comments</a> -->
                    </div>              
                  </div>

                </li>

                <li class="media">
                  <a class="pull-left">
                    <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kurafire/128.jpg" alt="profile">
                  </a>
                  <div class="media-body col-md-10">
                    <div class="well well-lg" style=" padding-bottom: 10px;">
                       <h3 class="media-heading">
                              <a href="profile" target="_parent"><strong>Marco Jacken</strong></a></h3>
                      <ul class="media-date text-uppercase reviews list-inline">
                        <span class="badge badge-important">Android</span>
                        <span class="badge badge-important">iOS</span>
                        <span class="badge badge-important">c++</span>
                      </ul>
                      <p class="media-comment">
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                      </p>
                      <!-- <a class="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
                      <a class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#replyOne"><span class="glyphicon glyphicon-comment"></span> 2 comments</a> -->
                    </div>              
                  </div>
                </li>

                <li class="media">
                  <a class="pull-left">
                    <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/lady_katherine/128.jpg" alt="profile">
                  </a>
                  <div class="media-body col-md-10">
                    <div class="well well-lg" style=" padding-bottom: 10px;">
                       <h3 class="media-heading">
                              <a href="profile" target="_parent"><strong>Marco Jacken</strong></a></h3>
                      <ul class="media-date text-uppercase reviews list-inline">
                        <span class="badge badge-important">Android</span>
                        <span class="badge badge-important">iOS</span>
                        <span class="badge badge-important">c++</span>
                      </ul>
                      <p class="media-comment">
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                      </p>
                      <!-- <a class="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
                      <a class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#replyOne"><span class="glyphicon glyphicon-comment"></span> 2 comments</a> -->
                    </div>              
                  </div>

                </li>

                <li class="media">
                  <a class="pull-left">
                    <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/lady_katherine/128.jpg" alt="profile">
                  </a>
                  <div class="media-body col-md-10">
                    <div class="well well-lg" style=" padding-bottom: 10px;">
                       <h3 class="media-heading">
                              <a href="profile" target="_parent"><strong>Marco Jacken</strong></a></h3>
                      <ul class="media-date text-uppercase reviews list-inline">
                        <span class="badge badge-important">Android</span>
                        <span class="badge badge-important">iOS</span>
                        <span class="badge badge-important">c++</span>
                      </ul>
                      <p class="media-comment">
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. 
                      </p>
                      <!-- <a class="btn btn-info btn-circle text-uppercase" href="#" id="reply"><span class="glyphicon glyphicon-share-alt"></span> Reply</a>
                      <a class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse" href="#replyOne"><span class="glyphicon glyphicon-comment"></span> 2 comments</a> -->
                    </div>              
                  </div>

                </li>
              </ul>




              <!--table footer-->
              <div class="row">
                <div class="col-md-8"><h5 style="margin-top: 25px;"><strong>Total Projects: 36</strong></h5>
                </div>
                <div class="col-md-4 ">
                  <nav aria-label="Page navigation">
                    <ul class="pagination">
                      <li>
                        <a href="#" aria-label="Previous">
                          <span aria-hidden="true">&laquo;</span>
                        </a>
                      </li>
                      <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li>
                        <a href="#" aria-label="Next">
                          <span aria-hidden="true">&raquo;</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
              <!-- table footer end -->
            </div>

          </div><!-- End row -->
        </div><!-- End container -->
      </div>
      <?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>