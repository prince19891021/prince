 
<?php $__env->startSection('nav_menu'); ?>
<!-- start navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
            <a class="navbar-brand" href="index.php">His <span>Win</span></a>
            <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse navbar_area">
            <ul class="nav navbar-nav navbar-right custom_nav">
                <li><a href="home">Home</a></li>
                <li><a href="post">Post</a></li>
                <li class="active"><a href="login">Login</a></li>
                <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Link one</a></li>
              <li><a href="#">Link Two</a></li>
              <li><a href="#">Link Three</a></li>
            </ul>
          </li>
          <li><a href="page.html">Page</a></li>
          <li><a href="blog-archive.html">Blog</a></li> -->
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<!-- End navbar -->
<?php $__env->stopSection(); ?> <?php $__env->startSection('service'); ?>

<div class="container">

    <div id="loginbox" style="margin-top:100px; margin-bottom: 80px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

        <div class="panel panel-info">

            <div class="panel-heading" style = "background: #06d0d8">
                <div class="panel-title" style = "color: #ffffff; font-weight:bold">Sign In</div>
                <div style="float:right; font-size: 100%; position: relative; top:-10px"><a href="#0" id="fogot_password" onClick="$('#loginbox').hide(); $('#forgetbox').show()" style = "color: #ffffff">Forgot password?</a></div>

            </div>


            <div style="padding-top:30px" class="panel-body">

                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                <form id="loginform" class="form-horizontal" role="form">
                    <div class="form-group">

                        <div class="col-md-12 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username or email">
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-12 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12 controls">

                        <div class="input-group inputGroupContainer">
                            <div class="checkbox">
                                <label> 
                                    <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                </label>
                            </div>

                        </div>
                        <div class="col-md-4 col-md-offset-10">
                            <button type="submit" class="btn custom-button-width" style = "background: #06d0d8; color: #ffffff">Sign In</button>

                        </div>
                        

                    </div>

                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:100%">
                                Don't have an account!
                                <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()" style = "color: #06d0d8">
                                     Sign Up Here
                                </a>
                            </div>
                        </div>
                    </div>


                </form>



            </div>

        </div>

    </div>

    <div id="signupbox" style="display:none; margin-top:100px; margin-bottom: 80px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

        <div class="panel panel-info">
            <div class="panel-heading" style = "background: #06d0d8; color: #ffffff">
                <div class="panel-title">Sign Up</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()" style = "color: #ffffff">Sign In</a></div>
            </div>

            <div style="padding-top:30px" class="panel-body">

                <form id="signupform" class="form-horizontal" role="form">


                    <div class="form-group">
                        <div class="col-md-12 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="signup_email" type="text" class="form-control" name="email" value="" placeholder="Email">
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-12 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="signup_firstname" type="text" class="form-control" name="username" value="" placeholder="UserName">
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-12 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="signup_password" type="text" class="form-control" name="password" value="" placeholder="Password">
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-12 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="signup_invitationcode" type="text" class="form-control" name="confirm_password" value="" placeholder="Confirm Password">
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-12 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
                                <select name="country" class="form-control selectpicker" placeholder="Please choose Country">
                            <option value=" " >China</option>
                            <option>Afghanistan</option>
                            <option>Angular</option>
                            <option>Canada</option>
                            <option>China</option>
                            <option>Japan</option>
                            <option>Russia</option>
                            <option>United Kingdom</option>
                            <option>United States</option>
                          </select>
                            </div>
                        </div>
                    </div><br>



                 <div class="form-group">
                        <div class="col-md-12 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
                                <select name="professional" class="form-control selectpicker" placeholder="Please choose Style">
                            <option>Personal</option>
                            <option>Company</option>
                          </select>
                            </div>
                        </div>
                    </div><br>


                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-4 col-md-offset-9">
                            <button type="submit" class="btn custom-button-width" style = "background: #06d0d8; color: #ffffff"> Sign Up</span></button>
                            
                        </div>
                    </div>

                </form>
            </div>

        </div>


    </div>
    <div id="forgetbox" style="display:none; margin-top: 100px; margin-bottom: 80px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

        <div class="panel panel-info">
            <div class="panel-heading" style = "background: #06d0d8; color: #ffffff">
                <div class="panel-title" style = "color: #ffffff">Forget Password</div>
                <div style="float:right; font-size: 100%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#forgetbox').hide(); $('#loginbox').show()" style = "color: #ffffff">Sign In</a></div>
            </div>

            <div class="panel-body">

                <form id="forgetform" class="form-horizontal" role="form">



                    <div class="form-group" style = "padding: 50px 50px;">
                        <div class="col-md-12 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="forget_email" type="text" class="form-control" name="email" value="" placeholder="Email">
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-4 col-md-offset-9">

                            <button type="submit" class="btn custom-button-width" style = "background: #06d0d8; color: #ffffff">Connect</span></button>

                        </div>
                    </div>




                </form>
            </div>

        </div>

    </div>
</div>


    



    






    <?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>