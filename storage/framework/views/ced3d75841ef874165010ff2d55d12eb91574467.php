 <?php $__env->startSection('nav_menu'); ?>
<!-- start navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="position: relative;">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
            <a class="navbar-brand" href="index.php">His <span>Win</span></a>
            <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse navbar_area">
            <ul class="nav navbar-nav navbar-right custom_nav">
                <li><a href="home">Home</a></li>
                <li class="active"><a href="post">Post</a></li>
                <li><a href="login">Login</a></li>
                <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Link one</a></li>
              <li><a href="#">Link Two</a></li>
              <li><a href="#">Link Three</a></li>
            </ul>
          </li>
          <li><a href="page.html">Page</a></li>
          <li><a href="blog-archive.html">Blog</a></li> -->
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<link href="<?php echo e(asset('css/round-about.css')); ?>" rel="stylesheet">
<!-- End navbar -->
<?php $__env->stopSection(); ?> <?php $__env->startSection('service'); ?>

<!-- Page Content -->
<div class="container">

    <!-- Introduction Row -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">About Us
                <!-- <small>It's Nice to Meet You!</small> -->
            </h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, explicabo dolores ipsam aliquam inventore corrupti
                eveniet quisquam quod totam laudantium repudiandae obcaecati ea consectetur debitis velit facere nisi expedita
                vel?</p>
        </div>
    </div>

    <!-- Team Members Row -->
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Our Support Team</h2>
        </div>
        <div class="col-lg-4 col-sm-6 text-center">
            <img class="img-circle img-responsive img-center" src="img/blogger.jpg" style="height: 200px; width: 200px;" alt="">
            <br>
            <h3 class="media-heading">
                <a href="#" target="_parent"><strong>Marco Jacken</strong><br></a><small>(Android)</small></h3>
            <p>What does this team member to? Keep it short! This is also a great spot for social links!</p>
        </div>
        <div class="col-lg-4 col-sm-6 text-center">
            <img class="img-circle img-responsive img-center" src="img/leify.png" style="height: 200px; width: 200px;" alt="">
            <br>
            <h3 class="media-heading">
                <a href="#" target="_parent"><strong>Marco Jacken</strong></a><br><small>(Android)</small></h3>
            <p>What does this team member to? Keep it short! This is also a great spot for social links!</p>
        </div>
        <div class="col-lg-4 col-sm-6 text-center" style="margin-bottom: 30px;">
            <img class="img-circle img-responsive img-center" src="img/3.png" style="height: 200px; width: 200px;" alt="">
            <br>
            <h3 class="media-heading">
                <a href="#" target="_parent"><strong>Marco Jacken</strong></a><br><small>(Android)</small></h3>

            <p>What does this team member to? Keep it short! This is also a great spot for social links!</p>
        </div>

        <div class="col-lg-4 col-sm-6 text-center">
            <img class="img-circle img-responsive img-center" src="img/gongchengshi.png" style="height: 200px; width: 200px;" alt="">
            <br>
            <h3 class="media-heading">
                <a href="#" target="_parent"><strong>Marco Jacken</strong></a><br><small>(Android)</small></h3>
            <p>What does this team member to? Keep it short! This is also a great spot for social links!</p>
        </div>
        <div class="col-lg-4 col-sm-6 text-center">
            <img class="img-circle img-responsive img-center" src="img/dev2.png" style="height: 200px; width: 200px;" alt="">
            <br>
            <h3 class="media-heading">
                <a href="#" target="_parent"><strong>Marco Jacken</strong></a><br><small>(Android)</small></h3>
            <p>What does this team member to? Keep it short! This is also a great spot for social links!</p>
        </div>
        <div class="col-lg-4 col-sm-6 text-center">
            <img class="img-circle img-responsive img-center" src="img/2.png" style="height: 200px; width: 200px;" alt="">
            <br>
            <h3 class="media-heading">
                <a href="#" target="_parent"><strong>Marco Jacken</strong></a><br><small>(Android)</small></h3>
            <p>What does this team member to? Keep it short! This is also a great spot for social links!</p>
        </div>
    </div>

    <h1 class="page-header"></h1>

    <!--table footer-->
    <div class="row">
        <div class="col-md-8">
            <h5 style="margin-top: 25px;"><strong>Total Projects: 36</strong></h5>
        </div>
        <div class="col-md-4 ">
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <!-- table footer end -->

</div>
<!-- /.container -->


<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>