 <?php $__env->startSection('nav_menu'); ?>
<!-- start navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
        aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      <a class="navbar-brand" href="index.php">His <span>Win</span></a>
      <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
    </div>
    <div id="navbar" class="navbar-collapse collapse navbar_area">
      <ul class="nav navbar-nav navbar-right custom_nav">
        <li class="active"><a href="home">Home</a></li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Job <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="post">Post Job</a></li>
              <li><a href="project_manage">My Jobs</a></li>
            </ul>
          </li>
        
        <li><a href="login">Login</a></li>
        <?php /* <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Link one</a></li>
              <li><a href="#">Link Two</a></li>
              <li><a href="#">Link Three</a></li>
            </ul>
          </li>
          <li><a href="page.html">Page</a></li>
          <li><a href="blog-archive.html">Blog</a></li> */ ?>
      </ul>
    </div>
    <!--/.nav-collapse -->
  </div>
</nav>
<!-- End navbar -->
<?php $__env->stopSection(); ?> <?php $__env->startSection('slider'); ?>
<!-- start slider section -->
<section id="sliderSection">
  <div class="mainslider_area">
    <!-- Start super slider -->
    <div id="slides">
      <ul class="slides-container">
        <!-- Start single slider-->
        <li>
          <img src="img/slider/1.jpg" alt="img">
          <div class="slider_caption">
            <h2><span>Welcome To</span> His Win</h2>
            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking
              at its layout.The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.Many
              desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search</p>
            <a class="slider_btn" href="post">Post Job Now!</a>
          </div>
        </li>
        <!-- Start single slider-->
        <li>
          <img src="img/slider/3.jpg" alt="img">
          <div class="slider_caption">
            <h2>Beautiful <span>Clear and Flexible</span></h2>
            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking
              at its layout.The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.Many
              desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search</p>
            <a class="slider_btn" href="post">Post Job Now!</a>
          </div>
        </li>
        <!-- Start single slider-->
        <li>
          <img src="img/slider/2.jpg" alt="img">
          <div class="slider_caption">
            <h2><span>Beausiness</span> & Corporate</h2>
            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking
              at its layout.The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.Many
              desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search</p>
            <a class="slider_btn" href="post">Post Job Now!</a>
          </div>
        </li>
      </ul>
      <nav class="slides-navigation">
        <a href="#" class="next"></a>
        <a href="#" class="prev"></a>
      </nav>
    </div>
  </div>
  </div>
</section>
<!-- End slider section -->
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('quote'); ?>
<section id="specialQuote">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 wow bounceInLeft">
        <p>You will get the best service from our site. Your success is my success!</p>
      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?> <?php $__env->startSection('content'); ?>

<div class="col-lg-3 col-md-6 col-sm-6">
  <div class="howworks_slider wow fadeInLeftBig">
    <h2 style = "text-align:center;">Development</h2>
    <div class="list-group"  style = "margin-top: 20px;">
    <a href="<?php echo e(('chenggong_list')); ?>" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <h2 class="mb-1">Android</h2>          
        </div>
        <p class="mb-1">You can find all android apps in this section.</p>
        <p style = "text-align: right; color: #06d0d8;">Get more</p>
      </a>
      <a href="<?php echo e(('chenggong_list')); ?>" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <h2 class="mb-1">iOS</h2>          
        </div>
        <p class="mb-1">There are many apps is developed by swift 2.2, swift 3.0 and objective-c. You can find iPhone, iOS Watch, iPad apps</p>
        <p style = "text-align: right; color: #06d0d8;">Get more</p>
      </a>
      <a href="<?php echo e(('chenggong_list')); ?>" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <h2 class="mb-1">Web Develop</h2>          
        </div>
        <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
        <p style = "text-align: right; color: #06d0d8;">Get more</p>
      </a>
    </div>
  </div>

  <div class="howworks_slider wow fadeInLeftBig">
    <h2 style = "text-align:center;">Job Type</h2>
    <div class="list-group" style = "margin-top: 20px;">
      <button class="button_project" style="vertical-align:middle"><span>Fixed Jobs </span></button>
    </div>
    <div class="list-group">
      <button class="button_project" style="vertical-align:middle"><span>Hourly Jobs </span></button>
    </div>
  </div>
</div>
<div class="col-lg-9 col-md-6 col-sm-6">
  <!-- start Our Team area -->
  <section id="ourTeam">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9">
          <div class="team_area wow fadeInRightBig">
            <div class="team_title">
              <h2>Good <span>Materials</span></h2>              
            </div>
            <a href ="<?php echo e(('chenggong_list')); ?>"><button class = "button button_more">Read More >>></button></a>
            <div class="team">
              <ul class="team_nav">
                <li>
                  <div class="team_img_sample">
                    <img src="img/app4.png" alt="team-img_sample">
                  </div>
                  <div class="team_content">
                    <a href = "<?php echo e(('chenggonganli')); ?>"><h4 class = "team_name">Android</h4></a>
                    <p>Belize City App</p>
                  </div>
                  <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
                </li>
                <li>
                  <div class="team_img_sample">
                    <img src="img/app2.png" alt="team-img_sample">
                  </div>
                  <div class="team_content">
                    <a href = "<?php echo e(('chenggonganli')); ?>"><h4 class="team_name">Swift 3.0</h4></a>
                    <p>Acroyoga App</p>
                  </div>
                  <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
                </li>
                <li>
                  <div class="team_img_sample">
                    <img src="img/app3.png" alt="team_img_sample">
                  </div>
                  <div class="team_content">
                    <a href = "<?php echo e(('chenggonganli')); ?>"><h4 class="team_name">Objective C</h4></a>
                    <p>Slogan App</p>
                  </div>
                  <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
                </li>
                <li>
                  <div class="team_img_sample">
                    <img src="img/app5.png" alt="team_img_sample">
                  </div>
                  <div class="team_content">
                    <a href = "<?php echo e(('chenggonganli')); ?>"><h4 class="team_name">Unity 3D</h4></a>
                    <p>Call of duty</p>
                  </div>
                  <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
                </li>
                <li>
                  <div class="team_img_sample">
                    <img src="img/app1.png" alt="team_img_sample">
                  </div>
                  <div class="team_content">
                    <a href = "<?php echo e(('chenggonganli')); ?>"><h4 class="team_name">WEB Dev</h4></a>
                    <p>Managing News</p>
                  </div>
                  <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9">
          <div class="team_area wow fadeInRightBig">
            <div class="team_title">
              <hr>
              <h2>Our <span>Service</span></h2>
            </div>
            <a href ="<?php echo e(('service_list')); ?>"><button class = "button button_more">Read More >>></button></a>
            <div class="team">
              <ul class="team_nav">
                <li>
                  <div class="team_img">
                    <img src="img/1.png" alt="team-img">
                  </div>
                  <div class="team_content">
                    <a href ="<?php echo e(('service_list')); ?>"><h4 class = "team_name">Ben Davis</h4></a>
                    <p>Managing Director</p>
                  </div>
                  <div class="team_social">
                    <a href="#"><span class="glyphicon glyphicon-earphone"></span></a>
                    <a href="#"><span class="glyphicon glyphicon-envelope"></span></a>
                    <a href="#"><span class="fa fa-skype"></span></a>
                    <a href="#"><span class="fa fa-smile-o"></span></a>
                  </div>
                </li>
                <li>
                  <div class="team_img">
                    <img src="img/blogger.jpg" alt="team-img">
                  </div>
                  <div class="team_content">
                    <a href ="<?php echo e(('service_list')); ?>"><h4 class="team_name">Dania Gerhardt</h4></a>
                    <p>Managing Director</p>
                  </div>
                  <div class="team_social">
                    <a href="#"><span class="glyphicon glyphicon-earphone"></span></a>
                    <a href="#"><span class="glyphicon glyphicon-envelope"></span></a>
                    <a href="#"><span class="fa fa-skype"></span></a>
                    <a href="#"><span class="fa fa-smile-o"></span></a>
                  </div>
                </li>
                <li>
                  <div class="team_img">
                    <img src="img/2.png" alt="team-img">
                  </div>
                  <div class="team_content">
                    <a href ="<?php echo e(('service_list')); ?>"><h4 class="team_name">Dania Gerhardt</h4></a>
                    <p>Managing Director</p>
                  </div>
                  <div class="team_social">
                    <a href="#"><span class="glyphicon glyphicon-earphone"></span></a>
                    <a href="#"><span class="glyphicon glyphicon-envelope"></span></a>
                    <a href="#"><span class="fa fa-skype"></span></a>
                    <a href="#"><span class="fa fa-smile-o"></span></a>
                  </div>
                </li>
                <li>
                  <div class="team_img">
                    <img src="img/leify.png" alt="team-img">
                  </div>
                  <div class="team_content">
                    <a href ="<?php echo e(('service_list')); ?>"><h4 class="team_name">Dania Gerhardt</h4></a>
                    <p>Managing Director</p>
                  </div>
                  <div class="team_social">
                    <a href="#"><span class="glyphicon glyphicon-earphone"></span></a>
                    <a href="#"><span class="glyphicon glyphicon-envelope"></span></a>
                    <a href="#"><span class="fa fa-skype"></span></a>
                    <a href="#"><span class="fa fa-smile-o"></span></a>
                  </div>
                </li>
                <li>
                  <div class="team_img">
                    <img src="img/3.png" alt="team-img">
                  </div>
                  <div class="team_content">
                    <a href ="<?php echo e(('service_list')); ?>"><h4 class="team_name">Dania Gerhardt</h4></a>
                    <p>Managing Director</p>
                  </div>
                  <div class="team_social">
                    <a href="#"><span class="glyphicon glyphicon-earphone"></span></a>
                    <a href="#"><span class="glyphicon glyphicon-envelope"></span></a>
                    <a href="#"><span class="fa fa-skype"></span></a>
                    <a href="#"><span class="fa fa-smile-o"></span></a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-9">
        <div class="team_area wow fadeInRightBig">
          <div class="team_title">
            <hr>
            <h2>Meet <span>Our Team</span></h2>
          </div>
          <a href ="<?php echo e(('developer_list')); ?>"><button class = "button button_more">Read More >>></button></a>
          <div class="team">
            <ul class="team_nav">
              <li>
                <div class="team_img_sample">
                  <img src="img/dev1.png" alt="team-img_sample">
                </div>
                <div class="team_content">
                  <a href ="<?php echo e(('profile')); ?>"><h4 class = "team_name">Android</h4></a>
                  <p>Belize City App</p>
                </div>
                <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
              </li>
              <li>
                <div class="team_img_sample">
                  <img src="img/dev2.png" alt="team-img_sample">
                </div>
                <div class="team_content">
                  <a href ="<?php echo e(('profile')); ?>"><h4 class="team_name">Swift 3.0</h4></a>
                  <p>Acroyoga App</p>
                </div>
                <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
              </li>
              <li>
                <div class="team_img_sample">
                  <img src="img/dev3.png" alt="team_img_sample">
                </div>
                <div class="team_content">
                  <a href ="<?php echo e(('profile')); ?>"><h4 class="team_name">Objective C</h4></a>
                  <p>Slogan App</p>
                </div>
                <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
              </li>
              <li>
                <div class="team_img_sample">
                  <img src="img/dev4.png" alt="team_img_sample">
                </div>
                <div class="team_content">
                  <a href ="<?php echo e(('profile')); ?>"><h4 class="team_name">Unity 3D</h4></a>
                  <p>Call of duty</p>
                </div>
                <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
              </li>
              <li>
                <div class="team_img_sample">
                  <img src="img/dev5.png" alt="team_img_sample">
                </div>
                <div class="team_content">
                  <a href ="<?php echo e(('profile')); ?>"><h4 class="team_name">WEB Dev</h4></a>
                  <p>Managing News</p>
                </div>
                <!--<div class="team_social">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-linkedin"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>
                  </div>-->
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
</div>
</section>
<!-- End Our Team area -->
</div>

<?php $__env->stopSection(); ?> <?php $__env->startSection('introduce'); ?>
<section id="service">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="service_area">
        <div class="service_title">
          <ul class="service_nav wow flipInX">
            <li>
              <a class="service_icon" href="#"><i class="fa fa-users"></i></a>
              <h2>An almost infinite number of project complete</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it
                to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged.</p>
              <a class="read_more" href="#">read more<i class="fa fa-long-arrow-right"></i></a>
            </li>
            <li>
              <a class="service_icon" href="#"><i class="fa fa-gears"></i></a>
              <h2>Efficient Workflow</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it
                to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged.</p>
              <a class="read_more" href="#">read more<i class="fa fa-long-arrow-right"></i></a>
            </li>
            <li>
              <a class="service_icon" href="#"><i class="fa fa-support"></i></a>
              <h2>Extraordinary Support</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it
                to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged.</p>
              <a class="read_more" href="#">read more<i class="fa fa-long-arrow-right"></i></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
</section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>