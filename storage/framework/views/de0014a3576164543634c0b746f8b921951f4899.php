  
  <?php $__env->startSection('content'); ?>
  
  <body class="login-img3-body">

    <div class="container">

      <form class="login-form" action="<?php echo url('login_admin'); ?>" method = "post">   
          <?php echo csrf_field(); ?>     
        <div class="login-wrap">
            <p class="login-img"><i class="icon_profile"></i></p>
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="email" name = "email" class="form-control" placeholder="Email" autofocus>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" name = "password" class="form-control" placeholder="Password">
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
            </label>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
            <?php /* <button class="btn btn-info btn-lg btn-block" type="submit">Signup</button> */ ?>
        </div>
      </form>
    </div>
</body>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>