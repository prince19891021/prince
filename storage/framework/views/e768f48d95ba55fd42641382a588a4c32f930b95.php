<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Creative - Bootstrap Admin Template</title>

    <!-- Bootstrap CSS -->    
    <?php echo Html::style('admin_style/css/bootstrap.min.css'); ?>

    <!-- bootstrap theme -->
    <?php echo Html::style('admin_style/css/bootstrap-theme.css'); ?>

    <!--external css-->
    <!-- font icon -->
    <?php echo Html::style('admin_style/css/elegant-icons-style.css'); ?>

    <?php echo Html::style('admin_style/css/font-awesome.min.css'); ?>  
    <!-- full calendar css-->
	  <?php echo Html::style('admin_style/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css'); ?>

    <?php echo Html::style('admin_style/assets/fullcalendar/fullcalendar/fullcalendar.css'); ?>

    <!-- easy pie chart-->
    <?php echo Html::style('admin_style/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css'); ?>

    <!-- owl carousel -->
    <?php echo Html::style('admin_style/css/owl.carousel.css'); ?>

    <?php echo Html::style('admin_style/css/jquery-jvectormap-1.2.2.css'); ?>

    <!-- Custom styles -->
    <?php echo Html::style('admin_style/css/fullcalendar.css'); ?>

    <?php echo Html::style('admin_style/css/widgets.css'); ?>

    <?php echo Html::style('admin_style/css/style.css'); ?>

    <?php echo Html::style('admin_style/css/style-responsive.css'); ?>

    <?php echo Html::style('admin_style/css/xcharts.min.css'); ?>

    <?php echo Html::style('admin_style/css/jquery-ui-1.10.4.min.css'); ?>

  </head>

  <body>
      <section id="container" class="">
          <?php if(Auth::guard('admin')->check()): ?>
            <?php echo $__env->make('admin.header.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('admin.sidebar.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->yieldContent('content'); ?>
          <?php else: ?>
            <?php echo $__env->yieldContent('content'); ?>
          <?php endif; ?>
       </section>

       <!-- javascripts -->
    <?php echo Html::script('admin_style/js/jquery.js'); ?>

    <?php echo Html::script('admin_style/js/jquery-ui-1.10.4.min.js'); ?>

    <?php echo Html::script('admin_style/js/jquery-1.8.3.min.js'); ?>

    <?php echo Html::script('admin_style/js/jquery-ui-1.9.2.custom.min.js'); ?>

    <!-- bootstrap -->
    <?php echo Html::script('admin_style/js/bootstrap.min.js'); ?>

    <!-- nice scroll -->
    <?php echo Html::script('admin_style/js/jquery.scrollTo.min.js'); ?>

    <?php echo Html::script('admin_style/js/jquery.nicescroll.js'); ?>

    <!-- charts scripts -->
    <?php echo Html::script('admin_style/assets/jquery-knob/js/jquery.knob.js'); ?>

    <?php echo Html::script('admin_style/js/jquery.sparkline.js'); ?>

    <?php echo Html::script('admin_style/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js'); ?>

    <?php echo Html::script('admin_style/js/owl.carousel.js'); ?>

    <!-- jQuery full calendar -->
    <!-- Full Google Calendar - Calendar -->
    <?php echo Html::script('admin_style/js/fullcalendar.min.js'); ?>

    <?php echo Html::script('admin_style/assets/fullcalendar/fullcalendar/fullcalendar.js'); ?>

    <!--script for this page only-->
    <?php echo Html::script('admin_style/js/calendar-custom.js'); ?>

    <?php echo Html::script('admin_style/js/jquery.rateit.min.js'); ?>

    <!-- custom select -->
    <?php echo Html::script('admin_style/js/jquery.customSelect.min.js'); ?>

    <?php echo Html::script('admin_style/assets/chart-master/Chart.js'); ?>

   
    <!--custome script for all page-->
    <?php echo Html::script('admin_style/js/scripts.js'); ?>

    <!-- custom script for this page-->
    <?php echo Html::script('admin_style/js/sparkline-chart.js'); ?>

    <?php echo Html::script('admin_style/js/easy-pie-chart.js'); ?>

    <?php echo Html::script('admin_style/js/jquery-jvectormap-1.2.2.min.js'); ?>

    <?php echo Html::script('admin_style/js/jquery-jvectormap-world-mill-en.js'); ?>

    <?php echo Html::script('admin_style/js/xcharts.min.js'); ?>

    <?php echo Html::script('admin_style/js/jquery.autosize.min.js'); ?>

    <?php echo Html::script('admin_style/js/jquery.placeholder.min.js'); ?>	
    <?php echo Html::script('admin_style/js/gdp-data.js'); ?>

    <?php echo Html::script('admin_style/js/morris.min.js'); ?>

    <?php echo Html::script('admin_style/js/sparklines.js'); ?>	
    <?php echo Html::script('admin_style/js/charts.js'); ?>

    <?php echo Html::script('admin_style/js/jquery.slimscroll.min.js'); ?>

  </body>
  </html>