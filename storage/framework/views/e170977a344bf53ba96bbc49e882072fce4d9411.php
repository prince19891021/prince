 
<?php $__env->startSection('nav_menu'); ?>
<!-- start navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="position: relative;">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
      aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.php">His <span>Win</span></a>
    <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
  </div>
  <div id="navbar" class="navbar-collapse collapse navbar_area" >
    <ul class="nav navbar-nav navbar-right custom_nav">
      <li><a href="home">Home</a></li>
      <li class="active"><a href="post">Post</a></li>
      <li><a href="login">Login</a></li>
                <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Link one</a></li>
              <li><a href="#">Link Two</a></li>
              <li><a href="#">Link Three</a></li>
            </ul>
          </li>
          <li><a href="page.html">Page</a></li>
          <li><a href="blog-archive.html">Blog</a></li> -->
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </nav>
  <!-- End navbar -->
  <link href="<?php echo e(asset('css/project_manage.css')); ?>" rel="stylesheet">
  <?php $__env->stopSection(); ?>
  <?php $__env->startSection('service'); ?>

  <div class="container">
    <div class="row">

<br>
        <div class="col-md-12">
          <h1>My jobs</h1>
          <p>A snippet I recently used to display homes for a local brokerage.  Focused more on images when accessed through mobile</p>
        </div>

    </div>
    
    <div class="row">

      <div class="col-md-12">
        <div class="widget blank no-padding">
          <div class="panel panel-default work-progress-table">
            <!-- Default panel contents -->

            <div class="panel-heading">
             <div class="row">             
               <div class="col-md-5">
                <h4>Total Spent Money: $42500</h4> 
              </div>
          </div>





        </div>
        <!-- Table -->
        <table id="mytable" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Number</th>
              <th>Job Name</th>
              <th>Budget</th>
              <th>Started Time</th>
              <th>Ended Time</th>
              <th style="width:25%">Progress</th>
              <th>Status</th>
              <th>Support Team</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>002101</td>
              <td>Development Website</td>
              <td>$2500</td>
              <td>2017-05-06</td>
              <td>present</td>
              <td>
                <div class="progress">
                  <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="red progress-bar">
                    <span>60%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-info">developing</span></td>
              <td>Kenia</td>
            </tr>
            <tr>
              <td>001115</td>
              <td>Android Dev</td>
              <td>$3400</td>
              <td>2016-06-12</td>
              <td>2017-02-03</td>
              <td>
                <div class="progress">
                  <div style="width: 100%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class="green progress-bar">
                    <span>100%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-primary">Ended</span></td>
            <td>Russia</td>
            </tr>
            <tr>
              <td>000572</td>
              <td>OCR Tech</td>
              <td>$14000</td>
              <td>2016-06-12</td>
              <td>2017-02-03</td>
              <td>
                <div class="progress">
                  <div style="width: 0%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="purple progress-bar">
                    <span>0%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-warning">Canceled</span></td>
              <td>Japan</td>
            </tr>
            <tr>
              <td>000241</td>
              <td>iOS development</td>
              <td>$400</td>
              <td>2016-06-12</td>
              <td> </td>
              <td>
                <div class="progress">
                  <div style="width: 0%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="purple progress-bar">
                    <span>0%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-danger">calling</span></td>
              <td>America</td>
            </tr>
            <tr>
              <td>002101</td>
              <td>Development Website</td>
              <td>$2500</td>
              <td>2017-05-06</td>
              <td>present</td>
              <td>
                <div class="progress">
                  <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="red progress-bar">
                    <span>60%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-info">developing</span></td>
              <td>Kenia</td>
            </tr>
            <tr>
              <td>002101</td>
              <td>Development Website</td>
              <td>$2500</td>
              <td>2017-05-06</td>
              <td>present</td>
              <td>
                <div class="progress">
                  <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="red progress-bar">
                    <span>60%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-info">developing</span></td>
              <td>Kenia</td>
            </tr>
            <tr>
              <td>002101</td>
              <td>Development Website</td>
              <td>$2500</td>
              <td>2017-05-06</td>
              <td>present</td>
              <td>
                <div class="progress">
                  <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="red progress-bar">
                    <span>60%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-info">developing</span></td>
              <td>Kenia</td>
            </tr>
            <tr>
              <td>002101</td>
              <td>Development Website</td>
              <td>$2500</td>
              <td>2017-05-06</td>
              <td>present</td>
              <td>
                <div class="progress">
                  <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="red progress-bar">
                    <span>60%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-info">developing</span></td>
              <td>Kenia</td>
            </tr>
            <tr>
              <td>002101</td>
              <td>Development Website</td>
              <td>$2500</td>
              <td>2017-05-06</td>
              <td>present</td>
              <td>
                <div class="progress">
                  <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="red progress-bar">
                    <span>60%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-info">developing</span></td>
              <td>Kenia</td>
            </tr>
            <tr>
              <td>002101</td>
              <td>Development Website</td>
              <td>$2500</td>
              <td>2017-05-06</td>
              <td>present</td>
              <td>
                <div class="progress">
                  <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="red progress-bar">
                    <span>60%</span>
                  </div>
                </div>
              </td>
              <td><span class="label label-info">developing</span></td>
              <td>Kenia</td>
            </tr>
              
          </tbody>
        </table>
        </div>

        <!--table footer-->
        <div class="row">
      <div class="col-md-9"><h5 style="margin-top: 25px;"><strong>Total Projects: 36</strong></h5>
      </div>
      <div class="col-md-3 ">
      <nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
      </div>
    </div>





      
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
      </div>
      <div class="modal-body">

       <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
       
     </div>
     <div class="modal-footer ">
      <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<!-- /.modal-dialog --> 
</div>
<script src="<?php echo e(asset('js/project_manage.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>