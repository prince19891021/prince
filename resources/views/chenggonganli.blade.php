
@extends('master') 
@section ('nav_menu')

<!-- start navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
            <a class="navbar-brand" href="index.php">His <span>Win</span></a>
            <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse navbar_area">
            <ul class="nav navbar-nav navbar-right custom_nav">
                <li><a href="home">Home</a></li>
                <li><a href="post">Post</a></li>
                <li class="active"><a href="login">Login</a></li>
                <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Link one</a></li>
              <li><a href="#">Link Two</a></li>
              <li><a href="#">Link Three</a></li>
            </ul>
          </li>
          <li><a href="page.html">Page</a></li>
          <li><a href="blog-archive.html">Blog</a></li> -->
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<!-- End navbar -->
@endsection 




  @section('service')



  <div class="container">

  <link rel="stylesheet" href="{{ asset('css/successcomment.css') }}">


    <div class="row">
      <div class="col-lg-12 col-md-12" style="margin-top:100px ;" id="div_img">
        <h2 align="center">成功案例名字</h2>
        
        <div class="col-lg-6 col-md-6" style="margin-top: 30px">
          <div class="service_title">
            <img src="img/shouji.png">
          </div>
        </div>

        <div class="col-lg-5 col-md-5" style="border: 1px solid; margin-top: 30px" id = "div_img2">
          <h2 align="center">项目描述</h2>
          
          <div class="col-lg-12 col-md-12" style="margin-top: 20px;  border: 1px solid；" >


            <textarea class="form-control" rows="15">

               大家可以用微信扫一扫案例里的图片二维
              码进行关注， 看看产品功能， 效果。

              本案例里面的这些图片， 每一张图片代表
              一个微信应用。

              由于本人以及朋友认识一些在银行， 保险
              等单位工作的人员。 经过介绍和一次合作。

              已经和相关银行， 保险公司达成长期合作共识。

              他们需要制作年报， 季度报或者互动游戏等等，
              优先考虑和我合作！  



            
            </textarea>

          </div>    
        </div>

      </div>

      




    <div class="comments-container" style="margin-top: 100px">
    <h1>项目评价 <a href="http://creaticode.com">creaticode.com</a></h1>

    <ul id="comments-list" class="comments-list">

      <li>
        <div class="comment-main-level">
          <!-- Avatar -->
          <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg" alt=""></div>
          <!-- Contenedor del Comentario -->
          <div class="comment-box">
            <div class="comment-head">
              <h6 class="comment-name by-author"><a href="http://creaticode.com/blog">金仁俊</a></h6>
              <span>hace 20 minutos</span>
              <i class="fa fa-reply"></i>
              <i class="fa fa-heart"></i>
            </div>
            <div class="comment-content">
              大家可以用微信扫一扫案例的图片二维码进行关注， 看看产品功能， 效果。
              本案例里面的这些图片， 每一张代表一个微信应用。
              由于本人以及朋友认识一些在银行，保险等单位工作的人员。 经过介绍和一次合作。 已经和相关银行， 保险公司达成长期合作共识。
              他们需要制造年报，季度报或者互动游戏等等，优先考虑和我合作！
            </div>
          </div>
       </li>

      <li>
        <div class="comment-main-level">
          <!-- Avatar -->
          <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg" alt=""></div>
          <!-- Contenedor del Comentario -->
          <div class="comment-box">
            <div class="comment-head">
              <h6 class="comment-name by-author"><a href="http://creaticode.com/blog">金仁俊</a></h6>
              <span>hace 20 minutos</span>
              <i class="fa fa-reply"></i>
              <i class="fa fa-heart"></i>
            </div>
            <div class="comment-content">
              大家可以用微信扫一扫案例的图片二维码进行关注， 看看产品功能， 效果。
              本案例里面的这些图片， 每一张代表一个微信应用。
              由于本人以及朋友认识一些在银行，保险等单位工作的人员。 经过介绍和一次合作。 已经和相关银行， 保险公司达成长期合作共识。
              他们需要制造年报，季度报或者互动游戏等等，优先考虑和我合作！
            </div>
          </div>
       </li>

      <li>
        <div class="comment-main-level">
          <!-- Avatar -->
          <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg" alt=""></div>
          <!-- Contenedor del Comentario -->
          <div class="comment-box">
            <div class="comment-head">
              <h6 class="comment-name by-author"><a href="http://creaticode.com/blog">金仁俊</a></h6>
              <span>hace 20 minutos</span>
              <i class="fa fa-reply"></i>
              <i class="fa fa-heart"></i>
            </div>
            <div class="comment-content">
              大家可以用微信扫一扫案例的图片二维码进行关注， 看看产品功能， 效果。
              本案例里面的这些图片， 每一张代表一个微信应用。
              由于本人以及朋友认识一些在银行，保险等单位工作的人员。 经过介绍和一次合作。 已经和相关银行， 保险公司达成长期合作共识。
              他们需要制造年报，季度报或者互动游戏等等，优先考虑和我合作！
            </div>
          </div>
       </li>

    </ul>

    </div> 

        
  </div>

  @endsection







