<!--Master Page-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Prince Site</title>


  <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>


  <!-- Bootstrap -->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- for fontawesome icon css file -->
  <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- superslides css -->
  <link rel="stylesheet" href="{{ asset('css/superslides.css') }}">
  <!-- for content animate css file -->
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <!-- slick slider css file -->
  <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
  <!-- custom css file -->
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
  <!-- website theme color file -->
  <link id="switcher" href="{{ asset('css/themes/cyan-theme.css') }}" rel="stylesheet">
  <!-- main site css file -->
  <link href="{{ asset('style.css') }}" rel="stylesheet">
  <!-- google fonts  -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">

    <!-- Bootstrap Validator css -->
  <link href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    


</head>

<body>
  <!-- =========================
    //////////////This Theme Design and Developed //////////////////////
    //////////// by www.wpfreeware.com======================-->

  <!-- Preloader -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>

  <!-- End Preloader -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>

  @yield('nav_menu')

  @yield('slider')  
<!-- start special quote -->  
  <!--@yield('quote')-->
  <!-- End special quote -->
  <!-- Start Service area -->
  <section id="service">
    @yield('service')
  </section>
  <!-- End Service area -->

  

  <!-- start How it works area -->
  <section id="ourTeam">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-10 col-sm-12">
          <div class="howworks_area">
            </div>
            <div class="row">
              @yield('content')
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End How it works area -->  

  

  <!-- start client testimonial -->
  @yield('other_quote')
  <!-- End client testimonial -->

 <!-- start client testimonial -->
  @yield('introduce')
  <!-- End client testimonial --> 

  <!-- start clients brand area -->
  <section id="clients_brand">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="clients_brand_area wow flipInX">
            <div class="client_title">
              <hr>
              <h2><span>Our</span> Clients</h2>
            </div>
            <div class="clients_brand">
              <!-- Start clients brand slider -->
              <ul class="clb_nav wow flipInX">
                <li><img src="img/envato-studio.png" alt="brand-img"></li>
                <li><img src="img/codecanyon.png" alt="brand-img"></li>
                <li><img src="img/audiojungle.png" alt="brand-img"></li>
                <li><img src="img/themeforest.png" alt="brand-img"></li>
                <li><img src="img/envato-studio.png" alt="brand-img"></li>
                <li><img src="img/codecanyon.png" alt="brand-img"></li>
                <li><img src="img/audiojungle.png" alt="brand-img"></li>
                <li><img src="img/themeforest.png" alt="brand-img"></li>
              </ul>
              <!-- End clients brand slider -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End clients brand area -->

  <!-- start footer -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="footer_bottom">
            <div class="copyright">
              <p>All right reserved </p>
            </div>
            <div class="developer">
              <p>Designed By <a href="http://wpfreeware.com/" rel="nofollow">Hiswin</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End footer -->




  <!-- jQuery Library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
  <!-- For content animatin  -->
  <script src="{{ asset('js/wow.min.js') }}"></script>
  <!-- bootstrap js file -->
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <!-- superslides slider -->
  <script src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ asset('js/jquery.animate-enhanced.min.js') }}"></script>
  <script src="{{ asset('js/jquery.superslides.min.js') }}" type="text/javascript" charset="utf-8"></script>
  <!-- slick slider js file -->
  <script src="{{ asset('js/slick.min.js') }}"></script>
  <!-- Google map -->
  <script src="https://maps.googleapis.com/maps/api/js"></script>
  <script src="{{ asset('js/jquery.ui.map.js') }}"></script>

  <!-- custom js file include -->
  <script src="{{ asset('js/custom.js') }}"></script>
  <script src="{{ asset('js/mine.js') }}"></script>

</body>

</html>


