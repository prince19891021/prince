@extends('master') @section ('nav_menu')
<!-- start navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="position: relative;">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
      aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
  </button>
  <a class="navbar-brand" href="index.php">His <span>Win</span></a>
  <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
</div>
<div id="navbar" class="navbar-collapse collapse navbar_area" >
    <ul class="nav navbar-nav navbar-right custom_nav">
      <li><a href="home">Home</a></li>
      <li class="active"><a href="post">Post</a></li>
      <li><a href="login">Login</a></li>
                <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Link one</a></li>
              <li><a href="#">Link Two</a></li>
              <li><a href="#">Link Three</a></li>
            </ul>
          </li>
          <li><a href="page.html">Page</a></li>
          <li><a href="blog-archive.html">Blog</a></li> -->
      </ul>
  </div>
  <!--/.nav-collapse -->
</div>
</nav>
<!-- End navbar -->
<link href="{{ asset('css/chenggonglist.css') }}" rel="stylesheet">
@endsection
@section('service')

<div class="container-fluid">
    <div class="container container-pad" id="property-listings">
        
        <div class="row">

          <div class="col-md-12">
            <h1>Our Excellent Projects</h1>
            <p>A snippet I recently used to display homes for a local brokerage.  Focused more on images when accessed through mobile</p>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-md-2" >
            <ul class="nav nav-pills nav-stacked">
              <li><a href="#">Android Development</a></li>
              <li><a href="#">iOS Development</a></li>
              <li class="active"><a href="#">Website Building</a></li>
              <li><a href="#">OCR Technology</a></li>
              <li><a href="#">Other Technology</a></li>
          </ul>
      </div>
      <div class="col-md-10">

       <!-- start project list row(table) -->             
       <div class="row">

        <div class="col-sm-6"> 

            <!-- Begin Listing: 609 W GRAVERS LN-->
            <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing " >
                <div class="media"  >
                    <a class="pull-left" href="#" target="_parent">
                        <img alt="image" class="img-responsive" src="img/app3.png"></a>
                        <div class="clearfix visible-sm"></div>
                        <div class="media-body fnt-smaller" >
                            <a href="#" target="_parent"></a>
                            <h4 class="media-heading">
                              <a href="#" target="_parent">Google Map Chattting app for android device</a></h4>
                              <h5 class="media-heading">$12,500</h5>
                              <span class="badge badge-important">Android</span>
                              <span class="badge badge-important">google map</span>
                              <span class="badge badge-important">firebase</span>
                              <p class="hidden-xs" style="margin-top: 15px;">Situated between fairmount
                                park and the prestigious philadelphia cricket
                                club, this beautiful 2+ acre property is truly
                                ...</p>
                            </div>
                        </div>
                    </div><!-- End Listing-->

                    <!-- Begin Listing: 609 W GRAVERS LN-->
                    <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing " >
                        <div class="media"  >
                            <a class="pull-left" href="#" target="_parent">
                                <img alt="image" class="img-responsive" src="img/app2.png"></a>
                                <div class="clearfix visible-sm"></div>
                                <div class="media-body fnt-smaller" >
                                    <a href="#" target="_parent"></a>
                                    <h4 class="media-heading">
                                      <a href="#" target="_parent">Google Map Chattting app for android device</a></h4>
                                      <h5 class="media-heading">$12,500</h5>
                                      <span class="badge badge-important">Android</span>
                                      <span class="badge badge-important">google map</span>
                                      <span class="badge badge-important">firebase</span>
                                      <p class="hidden-xs" style="margin-top: 15px;">Situated between fairmount
                                        park and the prestigious philadelphia cricket
                                        club, this beautiful 2+ acre property is truly
                                        ...</p>
                                    </div>
                                </div>
                            </div><!-- End Listing-->

                            <!-- Begin Listing: 609 W GRAVERS LN-->
                            <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing " >
                                <div class="media"  >
                                    <a class="pull-left" href="#" target="_parent">
                                        <img alt="image" class="img-responsive" src="img/app5.png"></a>
                                        <div class="clearfix visible-sm"></div>
                                        <div class="media-body fnt-smaller" >
                                            <a href="#" target="_parent"></a>
                                            <h4 class="media-heading">
                                              <a href="#" target="_parent">Google Map Chattting app for android device</a></h4>
                                              <h5 class="media-heading">$12,500</h5>
                                              <span class="badge badge-important">Android</span>
                                              <span class="badge badge-important">google map</span>
                                              <span class="badge badge-important">firebase</span>
                                              <p class="hidden-xs" style="margin-top: 15px;">Situated between fairmount
                                                park and the prestigious philadelphia cricket
                                                club, this beautiful 2+ acre property is truly
                                                ...</p>
                                            </div>
                                        </div>
                                    </div><!-- End Listing-->

                                    

                                </div>

                                <div class="col-sm-6">  

                                    <!-- Begin Listing: 609 W GRAVERS LN-->
                                    <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing " >
                                        <div class="media"  >
                                            <a class="pull-left" href="#" target="_parent">
                                                <img alt="image" class="img-responsive" src="img/app4.png"></a>
                                                <div class="clearfix visible-sm"></div>
                                                <div class="media-body fnt-smaller" >
                                                    <a href="#" target="_parent"></a>
                                                    <h4 class="media-heading">
                                                      <a href="#" target="_parent">Google Map Chattting app for android device</a></h4>
                                                      <h5 class="media-heading">$12,500</h5>
                                                      <span class="badge badge-important">Android</span>
                                                      <span class="badge badge-important">google map</span>
                                                      <span class="badge badge-important">firebase</span>
                                                      <p class="hidden-xs" style="margin-top: 15px;">Situated between fairmount
                                                        park and the prestigious philadelphia cricket
                                                        club, this beautiful 2+ acre property is truly
                                                        ...</p>
                                                    </div>
                                                </div>
                                            </div><!-- End Listing-->

                                            <!-- Begin Listing: 609 W GRAVERS LN-->
                                            <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing " >
                                                <div class="media"  >
                                                    <a class="pull-left" href="#" target="_parent">
                                                        <img alt="image" class="img-responsive" src="img/app1.png"></a>
                                                        <div class="clearfix visible-sm"></div>
                                                        <div class="media-body fnt-smaller" >
                                                            <a href="#" target="_parent"></a>
                                                            <h4 class="media-heading">
                                                              <a href="#" target="_parent">Google Map Chattting app for android device</a></h4>
                                                              <h5 class="media-heading">$12,500</h5>
                                                              <span class="badge badge-important">Android</span>
                                                              <span class="badge badge-important">google map</span>
                                                              <span class="badge badge-important">firebase</span>
                                                              <p class="hidden-xs" style="margin-top: 15px;">Situated between fairmount
                                                                park and the prestigious philadelphia cricket
                                                                club, this beautiful 2+ acre property is truly
                                                                ...</p>
                                                            </div>
                                                        </div>
                                                    </div><!-- End Listing-->

                                                    <!-- Begin Listing: 609 W GRAVERS LN-->
                                                    <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing " >
                                                        <div class="media"  >
                                                            <a class="pull-left" href="#" target="_parent">
                                                                <img alt="image" class="img-responsive" src="img/app2.png"></a>
                                                                <div class="clearfix visible-sm"></div>
                                                                <div class="media-body fnt-smaller" >
                                                                    <a href="#" target="_parent"></a>
                                                                    <h4 class="media-heading">
                                                                      <a href="#" target="_parent">Google Map Chattting app for android device</a></h4>
                                                                      <h5 class="media-heading">$12,500</h5>
                                                                      <span class="badge badge-important">Android</span>
                                                                      <span class="badge badge-important">google map</span>
                                                                      <span class="badge badge-important">firebase</span>
                                                                      <p class="hidden-xs" style="margin-top: 15px;">Situated between fairmount
                                                                        park and the prestigious philadelphia cricket
                                                                        club, this beautiful 2+ acre property is truly
                                                                        ...</p>
                                                                    </div>
                                                                </div>
                                                            </div><!-- End Listing-->
                                                        </div><!-- End Col -->
                                                    </div><!-- End project list row(table) -->
                                                    <!--table footer-->
                                                    <div class="row">
                                                      <div class="col-md-8"><h5 style="margin-top: 25px;"><strong>Total Projects: 36</strong></h5>
                                                      </div>
                                                      <div class="col-md-4 ">
                                                          <nav aria-label="Page navigation">
                                                              <ul class="pagination">
                                                                <li>
                                                                  <a href="#" aria-label="Previous">
                                                                    <span aria-hidden="true">&laquo;</span>
                                                                </a>
                                                            </li>
                                                            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                                            <li><a href="#">2</a></li>
                                                            <li><a href="#">3</a></li>
                                                            <li><a href="#">4</a></li>
                                                            <li><a href="#">5</a></li>
                                                            <li>
                                                              <a href="#" aria-label="Next">
                                                                <span aria-hidden="true">&raquo;</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                        <!-- table footer end -->
                                    </div>

                                </div><!-- End row -->
                            </div><!-- End container -->
                        </div>
                        @endsection