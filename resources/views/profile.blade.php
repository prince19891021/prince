@extends('master') @section ('nav_menu')
<!-- start navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="position: relative;">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
        aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
      <a class="navbar-brand" href="index.php">His <span>Win</span></a>
      <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
    </div>
    <div id="navbar" class="navbar-collapse collapse navbar_area">
      <ul class="nav navbar-nav navbar-right custom_nav">
        <li><a href="home">Home</a></li>
        <li class="active"><a href="post">Post</a></li>
        <li><a href="login">Login</a></li>
        <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Link one</a></li>
              <li><a href="#">Link Two</a></li>
              <li><a href="#">Link Three</a></li>
            </ul>
          </li>
          <li><a href="page.html">Page</a></li>
          <li><a href="blog-archive.html">Blog</a></li> -->
      </ul>
    </div>
  </nav>
  <!-- End navbar -->
  <link href="{{ asset('css/developer_profile.css') }}" rel="stylesheet">
  @endsection

@section('service')
<div class="container">
  <div class="row">
    <div class=" col-md-12" style="background: #6ecadc; padding: 0px;">


      <div class="col-sm-3 col-md-3 sidebar">
        <div class="sidebar_top">

          <img src="img/gongchengshi.png" alt="" />
          <h1 style = "text-align: right">Maxim Coval</h1>
        </div>
        <div class="details" style = "text-align: right">
          <h3>PHONE</h3>
          <p>+00 234 56 789</p>
          <h3>EMAIL</h3>
          <p><a href="mailto@example.com">mail@example.com</a></p>
          <address>
            <h3>Develop Tool</h3>
            <span>Android,</span>
            <span>iOS,</span>
            <span>Website.</span>
          </address>

        </div>
        <div class="clearfix"></div>
      </div>
      <!---->
      <link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
      <script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
      <!---//pop-up-box -->
      <div class="col-sm-9  col-md-9  main" style="background: #ffffff;">
        <div class="content">
          <div class="details_header">
            <ul>
              <li><a href="index.html"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>Resume</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>Print CV</a></li>
              <li><a href="contact.html"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>Email me</a></li>
              <li><a class="play-icon popup-with-zoom-anim" href="#small-dialog"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span>View photo</a></li>
              <div id="small-dialog" class="mfp-hide">
                <img src="img/images/g4.jpg" alt="" />
              </div>
              <script>
                $(document).ready(function () {
                  $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                  });

                });
              </script>
            </ul>
          </div>

          <div class="skills">
            <h3 class="clr">Professional skills</h3>
            <div class="skill_info">
              <p>Duis egestas tortor metus, vitae venenatis tortor tristique at. Pellentesque dignissim purus vitae enim blandit,
                sed tristique enim malesuada. Maecenas dolor erat, volutpat a tellus eu, euismod iaculis urna. Nulla dui
                purus, viverra viverra dolor non, malesuada dictum purus.</p>
            </div>
            <div class="skill_list">
              <div class="skill1">
                <h4>Software</h4>
                <ul>
                  <li>Photoshop</li>
                  <li>Flash</li>
                  <li>Dreemweeaver</li>
                  <li>In Design</li>
                </ul>
              </div>
              <div class="skill2">
                <h4>Languages</h4>
                <ul>
                  <li>HTML/CSS</li>
                  <li>ActionScript</li>
                  <li>PHP</li>
                  <li>Ruby on Rais</li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>


          <div class="company">
            <h3 class="clr">Completed Projects</h3>
            <div class="company_details">
              <h4>HengSiWei Website Building<span>May 2017 - PRESENT</span></h4>
              <h6>Website</h6>
              <div class="row">

                <div class="col-md-3">
                  <div class="team_img_sample">
                    <img src="img/app4.png" alt="team-img_sample">
                  </div>
                </div>
                <div class="col-md-9">
                  Nulla volutpat at est sed ultricies. In ac sem consequat, posuere nulla varius, molestie lorem. Duis quis nibh leo. Curabitur
                  a quam eu mi convallis auctor nec id mauris. Nullam mattis turpis eu turpis tincidunt, et pellentesque
                  leo imperdiet. Vivamus malesuada, sem laoreet dictum pulvinar, orci lectus rhoncus sapien, ut consectetur
                  augue nibh in neque. In tincidunt sed enim et tincidunt.
                </div>
              </div>

            </div>
            <p class="cmpny1">
              <div class="company_details">
                <h4>Company Name <span>NOVEMBER 2007 - MAY 2009</span></h4>
                <h6>WEB DESIGNER</h6>
                <div class="row">

                  <div class="col-md-3">
                    <div class="team_img_sample">
                      <img src="img/app4.png" alt="team-img_sample">
                    </div>
                  </div>
                  <div class="col-md-9">Nulla volutpat at est sed ultricies. In ac sem consequat, posuere nulla varius, molestie lorem. Duis quis
                    nibh leo. Curabitur a quam eu mi convallis auctor nec id mauris. Nullam mattis turpis eu turpis tincidunt,
                    et pellentesque leo imperdiet. Vivamus malesuada, sem laoreet dictum pulvinar, orci lectus rhoncus sapien,
                    ut consectetur augue nibh in neque. In tincidunt sed enim et tincidunt.
                  </div>
                </div>
              </div>
            </p>
          </div>
          <div class="education">
            <h3 class="clr">Education</h3>
            <div class="education_details">
              <h4>University of Awesome<span>JANUARY 2004 - OCTOBER 2009  来源:<a href="http://www.mycodes.net/" target="_blank">源码之家</a></span></h4>
              <h6>MAJOR PHD</h6>
              <p class="cmpny1">Nulla volutpat at est sed ultricies. In ac sem consequat, posuere nulla varius, molestie lorem. Duis quis nibh
                leo. Curabitur a quam eu mi convallis auctor nec id mauris. Nullam mattis turpis eu turpis tincidunt, et
                pellentesque leo imperdiet. Vivamus malesuada, sem laoreet dictum pulvinar, orci lectus rhoncus sapien, ut
                consectetur augue nibh in neque. In tincidunt sed enim et tincidunt.</p>
            </div>
            <div class="education_details">
              <h4>University of Techonology, Newyork <span>APRIL 2001 - SEPTEMBER 2003</span></h4>
              <h6>BACHELORS OF ARTS</h6>
              <p>Nulla volutpat at est sed ultricies. In ac sem consequat, posuere nulla varius, molestie lorem. Duis quis nibh
                leo. Curabitur a quam eu mi convallis auctor nec id mauris. Nullam mattis turpis eu turpis tincidunt, et
                pellentesque leo imperdiet. Vivamus malesuada, sem laoreet dictum pulvinar, orci lectus rhoncus sapien, ut
                consectetur augue nibh in neque. In tincidunt sed enim et tincidunt.</p>
            </div>
          </div>
          <div class="copywrite">
            <p>© 2015 Curriculum Vitae All Rights Reseverd </p>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection