<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Creative - Bootstrap Admin Template</title>

    <!-- Bootstrap CSS -->    
    {!! Html::style('admin_style/css/bootstrap.min.css') !!}
    <!-- bootstrap theme -->
    {!! Html::style('admin_style/css/bootstrap-theme.css') !!}
    <!--external css-->
    <!-- font icon -->
    {!! Html::style('admin_style/css/elegant-icons-style.css') !!}
    {!! Html::style('admin_style/css/font-awesome.min.css') !!}  
    <!-- full calendar css-->
	  {!! Html::style('admin_style/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css') !!}
    {!! Html::style('admin_style/assets/fullcalendar/fullcalendar/fullcalendar.css') !!}
    <!-- easy pie chart-->
    {!! Html::style('admin_style/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css') !!}
    <!-- owl carousel -->
    {!! Html::style('admin_style/css/owl.carousel.css') !!}
    {!! Html::style('admin_style/css/jquery-jvectormap-1.2.2.css') !!}
    <!-- Custom styles -->
    {!! Html::style('admin_style/css/fullcalendar.css') !!}
    {!! Html::style('admin_style/css/widgets.css') !!}
    {!! Html::style('admin_style/css/style.css') !!}
    {!! Html::style('admin_style/css/style-responsive.css') !!}
    {!! Html::style('admin_style/css/xcharts.min.css') !!}
    {!! Html::style('admin_style/css/jquery-ui-1.10.4.min.css') !!}
  </head>

  <body>
      <section id="container" class="">
          @if (Auth::guard('admin')->check())
            @include('admin.header.header')
            @include('admin.sidebar.sidebar')
            @yield('content')
          @else
            @yield('content')
          @endif
       </section>

       <!-- javascripts -->
    {!! Html::script('admin_style/js/jquery.js') !!}
    {!! Html::script('admin_style/js/jquery-ui-1.10.4.min.js') !!}
    {!! Html::script('admin_style/js/jquery-1.8.3.min.js') !!}
    {!! Html::script('admin_style/js/jquery-ui-1.9.2.custom.min.js') !!}
    <!-- bootstrap -->
    {!! Html::script('admin_style/js/bootstrap.min.js') !!}
    <!-- nice scroll -->
    {!! Html::script('admin_style/js/jquery.scrollTo.min.js') !!}
    {!! Html::script('admin_style/js/jquery.nicescroll.js') !!}
    <!-- charts scripts -->
    {!! Html::script('admin_style/assets/jquery-knob/js/jquery.knob.js') !!}
    {!! Html::script('admin_style/js/jquery.sparkline.js') !!}
    {!! Html::script('admin_style/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js') !!}
    {!! Html::script('admin_style/js/owl.carousel.js') !!}
    <!-- jQuery full calendar -->
    <!-- Full Google Calendar - Calendar -->
    {!! Html::script('admin_style/js/fullcalendar.min.js') !!}
    {!! Html::script('admin_style/assets/fullcalendar/fullcalendar/fullcalendar.js') !!}
    <!--script for this page only-->
    {!! Html::script('admin_style/js/calendar-custom.js') !!}
    {!! Html::script('admin_style/js/jquery.rateit.min.js') !!}
    <!-- custom select -->
    {!! Html::script('admin_style/js/jquery.customSelect.min.js') !!}
    {!! Html::script('admin_style/assets/chart-master/Chart.js') !!}
   
    <!--custome script for all page-->
    {!! Html::script('admin_style/js/scripts.js') !!}
    <!-- custom script for this page-->
    {!! Html::script('admin_style/js/sparkline-chart.js') !!}
    {!! Html::script('admin_style/js/easy-pie-chart.js') !!}
    {!! Html::script('admin_style/js/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('admin_style/js/jquery-jvectormap-world-mill-en.js') !!}
    {!! Html::script('admin_style/js/xcharts.min.js') !!}
    {!! Html::script('admin_style/js/jquery.autosize.min.js') !!}
    {!! Html::script('admin_style/js/jquery.placeholder.min.js') !!}	
    {!! Html::script('admin_style/js/gdp-data.js') !!}
    {!! Html::script('admin_style/js/morris.min.js') !!}
    {!! Html::script('admin_style/js/sparklines.js') !!}	
    {!! Html::script('admin_style/js/charts.js') !!}
    {!! Html::script('admin_style/js/jquery.slimscroll.min.js') !!}
  </body>
  </html>