@extends('master') @section ('nav_menu')
<!-- start navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
            <a class="navbar-brand" href="index.php">His <span>Win</span></a>
            <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse navbar_area" >
            <ul class="nav navbar-nav navbar-right custom_nav">
                <li><a href="home">Home</a></li>
                <li class="active"><a href="post">Post</a></li>
                <li><a href="login">Login</a></li>
                <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Link one</a></li>
              <li><a href="#">Link Two</a></li>
              <li><a href="#">Link Three</a></li>
            </ul>
          </li>
          <li><a href="page.html">Page</a></li>
          <li><a href="blog-archive.html">Blog</a></li> -->
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<!-- End navbar -->
@endsection @section('service')
<div class="container" style = "margin-top: 100px;">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="service_area">
          <div>
           <form class="form-horizontal" action=" " method="get" id="contact_form" action="/project_manage">
                <fieldset>

                    <!-- Form Name -->
                    <div class="form-group">
                        <div class="col-md-5 inputGroupContainer">
                            <h2 style = "color: #06d0d8; font-weight; bold">Posting the job</h2>
                        </div><br>
                    </div><br>

                    <!-- Text input-->

                    <div class="form-group">
                        <label class="col-md-4 control-label">Project Name</label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></span>
                                <input name="task_name" placeholder="Project Name" class="form-control" type="text">
                            </div>
                        </div>
                    </div><br>

                    <!-- Text input-->

                    <div class="form-group">
                        <label class="col-md-4 control-label">Project Type</label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
                                <select name="task_type" class="form-control selectpicker">
                            <option value=" " >Please choose type</option>
                            <option>Fixed</option>
                            <option>Hourly</option>
                          </select>
                            </div>
                        </div>
                    </div><br>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Development Tool</label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
                                <select name="task_tool" class="form-control selectpicker">
                            <option value=" " >Please choose development tool</option>
                            <option>Android</option>
                            <option>iOS</option>
                            <option>Website</option>
                            <option>OCR</option>
                          </select>
                            </div>
                        </div>
                    </div><br>


                    <!-- Text input-->

                    <div class="form-group">
                        <label class="col-md-4 control-label">Description</label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                <textarea class="form-control" name="task_description" placeholder="Description"></textarea>
                            </div>
                        </div>
                    </div><br>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Attachment</label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="">
                                <input type="file" > </button>
                            </div>
                        </div>
                    </div><br>

                    <!-- Text input-->

                    <div class="form-group">
                        <label class="col-md-4 control-label">Budget</label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                                <input name="budget" placeholder="$0.0" class="form-control" type="text">
                            </div>
                        </div>
                    </div><br>

                    <!-- Text input-->

                    <div class="form-group">
                        <label class="col-md-4 control-label">Contact Info</label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                <input name="phone_number" placeholder="(+86)555-1234" class="form-control" type="text">
                            </div>


                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input name="email_address" placeholder="aaa@gmail.com" class="form-control" type="text">
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-4 control-label">      </label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-skype"></i></span>
                                <input name="skype_id" placeholder="aaa@gmail.com" class="form-control" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">     </label>
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-smile-o"></i></span>
                                <input name="qq_id" placeholder="3225659784" class="form-control" type="text">
                            </div>
                        </div>
                    </div><br>

                    <!-- Success message -->
                    <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you
                        shortly.</div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-5">
                            <button type="submit" class="btn btn-lg btn-block" style = "background: #06d0d8; color: #ffffff">Post Job <span class="glyphicon glyphicon-send"></span></button>
                        </div>
                    </div>


                </fieldset>
            </form>
        </div>
      </div>
    </div>

<div class="container" style="margin-top: 100px;">
    <div class="col-lg-12 col-md-12 col-sm-12">
    </div>
</div>
<!-- End slider section -->
@endsection

